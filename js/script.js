// Hamburger menu function.
function toggleMenu(event) {
  // console.log('Menu clicked');
  // Get a nav bar.
  let navBar = document.getElementById("main-nav");
  let expanded = event.currentTarget.getAttribute("aria-expanded");
  console.log(expanded);

  // Check if it is hidden.
  if (expanded === "true") {
    // Update the menu visibility.
    navBar.classList.add("closed");
    navBar.classList.remove("opened");
    // Update the button state.
    event.currentTarget.setAttribute('aria-expanded', 'false');
  } else {
    navBar.classList.add("opened");
    navBar.classList.remove("closed");
    event.currentTarget.setAttribute('aria-expanded', 'true');
  }

}

document.getElementById('menu-top-button').addEventListener('click', toggleMenu)
